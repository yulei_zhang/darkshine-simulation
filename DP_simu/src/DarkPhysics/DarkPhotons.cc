// This is a class to simulate dark photons A production by electrons in matter eN -> eNA
// Description is in the base class DarkMatter code
// To be used in a Geant4 application.
//
//
#include "DarkPhysics/DarkMatter.hh"
#include "DarkPhysics/DarkPhotons.hh"

#include "G4ios.hh"
#include <cstdlib>
#include <cmath>
#include <iostream>

#include <gsl/gsl_math.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>
#include <gsl/gsl_monte_miser.h>
#include <gsl/gsl_monte_vegas.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_rng.h>

#include "G4SystemOfUnits.hh"

#define EPSPARINV 1.e-8
#define  nMALowM 18 // number of MA grid divisions

double TotCSVectorParticle(double MAtest) // CS in GeV^-2 for epsilon=1
{
//    double MMAA[nMALowM] = {0.000001, 0.00001, 0.00002, 0.00003, 0.00004, 0.00005, 0.00006, 0.00007, 0.0001, 0.00015,
//                            0.0002,
//                            0.0003, 0.0004, 0.0005, 0.0006, 0.0007, 0.0008, 0.0009}; // mass of A' in GeV
//    double TotCSList[nMALowM] = {831989., 714214., 608205., 537442., 485739., 445566., 413065., 386006., 325514.,
//                                 261274., 219140.,
//                                 165447., 131948., 108852., 91940.7, 79039.6, 68896.5, 60733.3};
    double MMAA[nMALowM] = {0.00158489, 0.00251189, 0.00398107, 0.00630957, 0.01, 0.0158489, 0.0251189, 0.0398107,
                            0.0630957, 0.1, 0.158489, 0.251189, 0.398107, 0.630957, 1, 1.58489, 2.51189}; // mass of A' in GeV
    double TotCSList[nMALowM] = {8.05E+12, 4.74E+12, 2.63E+12, 1.38E+12, 6.69E+11, 2.89E+11, 1.08E+11, 3.65E+10,
                                 1.15E+10, 3.36E+09, 9.05E+08, 2.15E+08, 4.08E+07, 5.01E+06, 292608, 13118.8, 571.706
    };

    return parinv(MAtest, MMAA, TotCSList, nMALowM); // This is to be converted to pb and multiplied by eps^2
}

DarkPhotons::DarkPhotons(double MAIn, double EThreshIn, double SigmaNormIn, double ANuclIn, double ZNuclIn,
                         double DensityIn,
                         double epsilIn, int IDecayIn)
        : DarkMatter(MAIn, EThreshIn, SigmaNormIn, ANuclIn, ZNuclIn, DensityIn, epsilIn, IDecayIn) {
    //DMType = 1;
    ParentPDGID = 11;
    DaughterPDGID = 11;
    std::cout << "Initialized DarkPhotons off electrons and positrons for material density = " << DensityIn
              << std::endl;
    std::cout << std::endl;
}


DarkPhotons::~DarkPhotons() { ; }

double DarkPhotons::TotalCrossSectionCalc(double E0) {
    //double ThetaMaxA;
    //double ThetaMaxEl;
    double sigmaTot;

    if (E0 < 2. * MA) return 0.;

    if (MA > 0.001) { // analytical IWW calculation above 1 MeV, result in pb

        //ThetaMaxA = pow(MA/E0, 1.5);
        //ThetaMaxEl = sqrt(MA*Mel)/E0;

        //begin: chi-formfactor calculation

        gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000);

        double result, error;
        double tmin = MA * MA * MA * MA / (4. * E0 * E0);
        double tmax = MA * MA;

        gsl_function F;
        ParamsForChi alpha = {1.0, 1.0, 1.0, 1.0};
        F.function = &chi;
        F.params = &alpha;

        alpha.AA = ANucl;
        alpha.ZZ = ZNucl;
        alpha.MMA = MA;
        alpha.EE0 = E0;

        gsl_integration_qags(&F, tmin, tmax, 0, 1e-7, 1000, w, &result, &error);

        //printf ("chi/Z^2 = % .18f\n", result/(ZNucl*ZNucl));
        //printf ("result    = % .18f\n", result);
        //printf ("estimated error = % .18f\n", error);
        //printf ("intervals =  %d\n", w->size);

        double ChiRes = result;

        gsl_integration_workspace_free(w);

        //end: chi-formfactor calculation

        double beta = sqrt(1. - MA * MA / (E0 * E0));
        double cutoff1 = Mel / MA;
        double cutoff2 = MA / E0;
        double cutoff = cutoff2;
        if (cutoff1 > cutoff2) cutoff = cutoff1;
        sigmaTot = GeVtoPb * (4. / 3.) * alphaEW * alphaEW * alphaEW * epsilBench * epsilBench * ChiRes * beta *
                   log(1. / (cutoff * cutoff)) / (MA * MA);
        if (sigmaTot < 0.) sigmaTot = 0.;

        //double KFactor = KfactorApproximate(MA, E0);

        G4cout<<"MA: "<<MA<<", E0: "<<E0<<", Cutoff: " << cutoff <<", sigmaTot: "<<sigmaTot<<G4endl;

        G4cout << "Total CS calc, E, M, K = " << E0 << " " << MA << " " << G4endl; //KFactor << G4endl;

        return sigmaTot; // / KFactor; // This K-factor decreases the cross section for MA > ~2 MeV


    } else {

        G4cout << "No analytical calculations below 1 MeV, exiting" << G4endl;
        exit(1);
    }
}

double DarkPhotons::GetSigmaTot(double E0) {
    if (MA > 0.001 ) {
        return GetSigmaTot0(E0);
    } else {
        //return TotCSVectorParticle(MA) * GeVtoPb * epsilBench * epsilBench;
        return TotCSVectorParticle(MA) * epsilBench * epsilBench;
    }
}


double DarkPhotons::CrossSectionDSDX(double XEv, double E0) {
    double sigma = 0;
    if (MA > 0.001) {
        double momentumOfDP = sqrt(XEv * XEv * E0 * E0 - MA * MA);
        double umaxtilde = -MA * MA * (1.0 - XEv) / XEv - Mel * Mel * XEv;
        double Numerator =
                Mel * Mel * XEv * (-2. + 2. * XEv + XEv * XEv) - 2. * umaxtilde * (3. - 3. * XEv + XEv * XEv);
        double Denominator = 3. * XEv * umaxtilde * umaxtilde;
        sigma = momentumOfDP * Numerator / Denominator;
        return sigma;
    } else {
        return sigma;
    }
}


double DarkPhotons::CrossSectionDSDXDU(double XEv, double UThetaEv, double E0) {
    double sigma = 0;
    if (MA > 0.001) {
        double Uxtheta = 2. * E0 * E0 * UThetaEv * XEv + MA * MA * (1.0 - XEv) / XEv + Mel * Mel * XEv;
        double AA = (1. - XEv + XEv * XEv / 2.) / (Uxtheta * Uxtheta);
        double BB = (1. - XEv) * (1. - XEv) * MA * MA / (Uxtheta * Uxtheta * Uxtheta * Uxtheta);
        double CC = MA * MA - Uxtheta * XEv / (1. - XEv);
        sigma = XEv * (AA + BB * CC);
        return sigma;
    } else {
        return sigma;
    }
}


double DarkPhotons::Width() {
    return 1. / 3. * 1. / 137. * MA * epsil * epsil * sqrt(1. - 4. * Mel * Mel / (MA * MA)) *
           (1. + 2. * Mel * Mel / (MA * MA));
}
